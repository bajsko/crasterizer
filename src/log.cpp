/* qlog.cpp
 *
 * Description: Provides functions to log information to different files/
 * buffers. 
 *
 * Created by Klas Henriksson on 2019/08/20. All right reserved.
 */

#include "log.h"
#include <stdlib.h>
#include <cstdio>

static FILE* s_mainFile = NULL;

void CrastLogInit()
{
    s_mainFile = fopen("main.log", "wb");
    if(!s_mainFile)
        abort();
}

void CrastLog(const int& mode, const qstring& msg)
{
    switch(mode)
    {
        case kLogModeMain:
            {
                fwrite(msg.c_str(), 1, msg.length(), s_mainFile);
                fflush(s_mainFile);
            }

        default:
            break;
    }
}

void CrastLog(const int& mode, const char* format, ...)
{
    char buffer[256] = {0};
    va_list args;
    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer), format, args);
    va_end(args);

    switch(mode)
    {
        case kLogModeMain:
            {
                fwrite(buffer, 1, strlen(buffer), s_mainFile);
                fflush(s_mainFile);
            }

        default:
           break; 
    }
}
