/*
 * crasterizer
 * main.cpp
 *
 * Description: entry point of the rasterizer, writing bmp's
 *
 * Created by Klas Henriksson on 2019/08/19. All right reserved.
 */

#include "log.h"
#include "renderer.h"

int writeCString(const char* str, char* buffer)
{
    const char* p = str;
    while(*(p))
    {
        buffer[p-str] = *p;
        p++;
    }

    return (int)(p-str);
}

int main(int argc, char** argv)
{
    CrastLogInit();
    Renderer renderer;
    renderer.setScreenDimensions(800,600);
    renderer.windingOrder = WO_CCW;
    renderer.renderMode = RM_WIRE;
    renderer.renderCamera.projectionMat = Q_ortho(-16,16,-4.0f/3.0f*16, 4.0f/3.0f*16, 0.1, 100);
    renderer.renderCamera.viewMat = Q_lookAt(vec3f(5,50,-5), vec3f(5,0,-5), vec3f(0,0,-1));

    std::vector<Vertex> verts;
    verts.push_back(Vertex(vec3f(-10.f,0, -10), vec2f(0,0), vec4f(1,0.1f,1,1)));
    verts.push_back(Vertex(vec3f(-13,0,0), vec2f(0,1), vec4f(1,1,0.3f,1)));
    verts.push_back(Vertex(vec3f(10,0,-19), vec2f(1,1), vec4f(0.7f,0.1f,1,1)));
    verts.push_back(Vertex(vec3f(-15.0f, 10, 0), vec2f(0,0), vec4f(0.5f, 0.5f, 0, 1)));
    verts.push_back(Vertex(vec3f(-12.0f, 0, 0), vec2f(0,1), vec4f(0.2f, 0.1f, 0.7f, 1)));
    verts.push_back(Vertex(vec3f(-10.0f, 0, -10), vec2f(1,1), vec4f(0, 0.7f, 0.9f, 1)));

    renderer.enable(kCRasterizer16SampleAntiAliasBit);
    renderer.clearColor(0,0,0,0);
    renderer.clear(kCRasterizerColorBufferBit | kCRasterizerDepthBufferBit);
    renderer.fixedRasterize(verts);

    //output buffers
    
    const RenderBuffer& buff = renderer.getRenderBuffer();
    FILE* file = fopen("colorbuff.ppm", "w");
    char header[26] = {0};
    int o1 = 0,o2 = 0;
    header[0] = 'P';
    header[1] = '6';
    header[2] = ' ';
    o1 = writeCString(std::to_string(buff.width).c_str(), header+3);
    header[3+o1] = ' ';
    o2 = writeCString(std::to_string(buff.height).c_str(), header+3+o1+1);    
    writeCString("\n255\n", header+3+o1+o2+1);

    char rgb[3] = {0};
    fwrite(header, 1, strlen(header), file);

    for(int y = 0; y < buff.height; y++) 
    {
        for(int x = 0; x < buff.width; x++)
        {
            const vec4f& color = buff.colorBuffer[x+y*buff.width];
            rgb[0] = (char)(color.x * 255.0f);
            rgb[1] = (char)(color.y * 255.0f);
            rgb[2] = (char)(color.z * 255.0f);
            fwrite(rgb, 1, sizeof(rgb), file);
        }
    }

    fclose(file);

    file = fopen("depthbuff.ppm", "w");
    fwrite(header, 1, strlen(header), file);
    for(int y = 0; y < buff.height; y++)
    {
        for(int x = 0; x < buff.width; x++)
        {
            real depth = -buff.depthBuffer[x+y*buff.width];
            depth /= (renderer.renderCamera.far-renderer.renderCamera.near);
            depth = clampf(1 - depth,0,1);
            rgb[0] = (char)(255.0f * depth);
            rgb[1] = (char)(255.0f * depth);
            rgb[2] = (char)(255.0f * depth);
            fwrite(rgb, 1, sizeof(rgb), file);
        }
    }

    fclose(file);

    CrastLog(kLogModeMain, "All done! Bye!\n");
    return 0;
}
