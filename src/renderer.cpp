/* crasterizer
* renderer.cpp
 *
 * Description: The renderer is the main module of the crasterizer. The user
 * pushes vertices foring triangles to the renderer which then rasterizes them
 * to the screen buffers (depth & color) 
 *
 * Created by Klas Henriksson on 2019/08/19. All right reserved.
 */

#include "renderer.h"
#include "log.h"

Renderer::Renderer() : renderCamera(), m_viewport(0,0,800,600),
    m_buffer(800, 600), windingOrder(WO_CCW), renderMode(RM_FILL), modelMat(),
    m_flags(0) 
{
    CrastLog(kLogModeMain, "Renderer::Renderer Initialized Renderer\n");    
}

void Renderer::setScreenDimensions(const int& w, const int& h)
{
    if(w*h > m_buffer.width*m_buffer.height)
    {
        m_buffer.colorBuffer = std::unique_ptr<vec4f[]>(new vec4[w*h]);
        m_buffer.depthBuffer = std::unique_ptr<float[]>(new float[w*h]);
        CrastLog(kLogModeMain, "Renderer::setScreenDimensions Reallocated screen buffers (" + 
                std::to_string(w) + "x" + std::to_string(h) + "\n");
    }

    m_buffer.width = w;
    m_buffer.height = h;
    CrastLog(kLogModeMain, "Renderer::setScreenDimensions Resized screen buffers "
            "(%dx%d)\n", w,h);
}

void Renderer::setViewport(const int& x, const int& y, const int& w, const int& h)
{
    m_viewport.x = clampf(x,0,m_buffer.width);
    m_viewport.y = clampf(y,0,m_buffer.height);
    m_viewport.width = clampf(w, 0, m_buffer.width-m_viewport.x);
    m_viewport.height = clampf(h, 0, m_buffer.height - m_viewport.y);
    CrastLog(kLogModeMain, "Renderer::setViewport top-left: (%.2fx%.2f), dim: (%.2fx%.2f)\n", 
            m_viewport.x, m_viewport.y, m_viewport.width, m_viewport.y);
}

void Renderer::setViewport(const Rectangle& r)
{
    return setViewport(r.x, r.y, r.width, r.height);
}

void Renderer::clear(const int& mode)
{
    bool clr = mode & kCRasterizerColorBufferBit;
    bool depth = mode & kCRasterizerDepthBufferBit;

    for(int i = 0; i < m_buffer.width*m_buffer.height; i++)
    {
        if(clr)
        {
            vec4f* colorPoint = m_buffer.colorBuffer.get() + i;
            colorPoint->x = m_clearColor.x;
            colorPoint->y = m_clearColor.y;
            colorPoint->z = m_clearColor.z;
        }

        if(depth)
        {
           float* depthPoint = m_buffer.depthBuffer.get() + i;
           *depthPoint = -renderCamera.far-1.0f;
        }
    }
}

void Renderer::enable(const int& mode)
{
    m_flags |= mode;
}

void Renderer::disable(const int& mode)
{
    m_flags &= (~mode);
}

float Renderer::edgeFunction(const vec2f& a, const vec2f& b, const vec2f& p)
{
    vec2f A = p-a;
    vec2f B = b-a;

    float det = A.x*B.y - A.y*B.x;
    return windingOrder == WO_CCW ? det : -det;
}

float Renderer::edgeFunction(const vec3f& a, const vec3f& b, const vec2f& p)
{
    return edgeFunction(vec2f(a.x, a.y), vec2f(b.x,b.y), p);
}

float Renderer::edgeFunction(const vec3f& a, const vec3f& b, const vec3f& p)
{
    return edgeFunction(vec2f(a.x, a.y), vec2f(b.x,b.y), vec2f(p.x, p.y));
}

bool Renderer::intersectsTriangle(const vec2f& p, const Triangle& triangle, vec3f* const& baycentric)
{
    float A_v0v1p = edgeFunction(triangle.v0.position, triangle.v1.position, p);
    float A_v1v2p = edgeFunction(triangle.v1.position, triangle.v2.position, p);
    float A_v2v0p = edgeFunction(triangle.v2.position, triangle.v0.position, p);
    float A_v0v1v2 = edgeFunction(triangle.v0.position, triangle.v1.position, triangle.v2.position);
    
    if(A_v0v1p >= 0 && A_v1v2p >= 0 && A_v2v0p >= 0)
    {
        if(renderMode == RM_WIRE)
        {
            int j = 0;
            float dx[3] = {A_v0v1p, A_v1v2p, A_v2v0p};
            for(int i = 0; i < 3; i++)
            {
                float c = dx[i];
                float u = dx[(i+1) % 3];
                float v = dx[(i+2) % 3];
                if((fabsf(c/u) > 0.0175f && fabsf(c/v) > 0.0175f))
                    j++;
            }

            if(j == 3)
                return false;
        }

        if(baycentric)
        {
            baycentric->x = fabsf(A_v1v2p/A_v0v1v2);
            baycentric->y = fabsf(A_v2v0p/A_v0v1v2);
            baycentric->z = fabsf(A_v0v1p/A_v0v1v2);
        }
        return true;
    }

    return false;
}

void Renderer::perspCorrectVertexAttributes(Vertex& v)
{
#ifdef PERSP_CORRECT_INTERP
    v.color *= (1.0f/v.position.z);
    v.texcoord *= (1.0f/v.position.z);
#endif
    v.position.z = 1.0f / v.position.z;
}

Rectangle Renderer::computeBoundingRect(const Triangle& triangle)
{
    float xmin = 9999999;
    float ymin = 9999999;
    float xmax = -9999999;
    float ymax = -9999999;

    vec3f pos[3] = { triangle.v0.position, triangle.v1.position,
        triangle.v2.position };
    for(int i = 0; i < 3; i++)
    {
        if(pos[i].x < xmin)
            xmin = pos[i].x;
        if(pos[i].x > xmax)
            xmax = pos[i].x;
        if(pos[i].y < ymin)
            ymin = pos[i].y;
        if(pos[i].y > ymax)
            ymax = pos[i].y;
    }

    return Rectangle(xmin,ymin,xmax-xmin,ymax-ymin);
}

void Renderer::convertToRasterSpace(vec3f& p)
{
    float zCamSpace = ((renderCamera.viewMat * modelMat).multVec(p)).z;

    mat44 proj = renderCamera.projectionMat;
    p = ((proj * renderCamera.viewMat * modelMat).multVec(p));
    CrastLog(kLogModeMain, "Renderer::convertToRasterSpace vert coord NDC space: (%.2f, %.2f, %.2f)\n", p.x,p.y,p.z);
        
    p.x = m_viewport.x + (1 + p.x)*0.5f*m_viewport.width;
    p.y = -(1 + p.y)*0.5f*m_viewport.height + m_viewport.y + m_viewport.height; 
    p.z = zCamSpace;
    CrastLog(kLogModeMain, "Renderer::convertToRasterSpace vert coord raster space: (%.2f, %.2f, %.2f)\n", p.x,p.y,p.z);
}

int Renderer::fixedRasterize(const std::vector<Vertex>& vertices)
{
    if(vertices.size() % 3 != 0)
    {
        CrastLog(kLogModeMain, "[ERROR] Renderer::fixedRasterize - vertices are"
                " not a power of 3. Rasterization failed.\n");
        return CRAST_FAIL;
    }

    std::vector<Triangle> triangles;

    std::vector<Vertex>::const_iterator it = vertices.begin();
    while(it != vertices.end())
    {
        const Vertex& v0 = *it;
        const Vertex& v1 = *(it+1);
        const Vertex& v2 = *(it+2);
        triangles.push_back(Triangle(v0,v1,v2));
        it += 3;
    }

    CrastLog(kLogModeMain, "Renderer::fixedRasterize - Beginning processing %d"
            " triangles.\n", (int)triangles.size());

    std::vector<Triangle>::iterator triangleIt = triangles.begin();
    while(triangleIt != triangles.end())
    {
        Triangle& t = (*triangleIt);

        convertToRasterSpace(t.v0.position);
        convertToRasterSpace(t.v1.position);
        convertToRasterSpace(t.v2.position);

        perspCorrectVertexAttributes(t.v0);
        perspCorrectVertexAttributes(t.v1);
        perspCorrectVertexAttributes(t.v2);

        Rectangle bbox = computeBoundingRect(t);
        bbox.x = clampf(bbox.x, 0, m_buffer.width);
        bbox.y = clampf(bbox.y, 0, m_buffer.height);
        bbox.width = clampf(bbox.width, 0, m_buffer.width-bbox.x);
        bbox.height = clampf(bbox.height, 0, m_buffer.height-bbox.y);

        for(int x = bbox.x; x < bbox.x + bbox.width; x++)
        {
            for(int y = bbox.y; y < bbox.y + bbox.height; y++)
            {
                writeBuffers(x,y,t);
            }
        } 
        
        triangleIt++;
    }

    return CRAST_OK; 
}

void Renderer::writeBuffers(const int& x, const int& y, const Triangle& t)
{
    vec2f p;
    int index = x + y * m_buffer.width;
    vec4f clr;
    vec2f texcoord;
    vec3f baycentric;

    vec4f finalClr;
    float finalDepth;

    int aliasAmount = renderMode == RM_WIRE ? 1 : (m_flags & kCRasterizer4SampleAntiAliasBit) ? 4 : 
       (m_flags & kCRasterizer8SampleAntiAliasBit) ? 8 : (m_flags & kCRasterizer16SampleAntiAliasBit) ?
       16 : 1;

    //check first if the pixel passes depth test
    p = vec2f(x+0.5f, y+0.5f);
    if(intersectsTriangle(p,t,&baycentric))
    {
         finalDepth = 1.0f / (baycentric.x * t.v0.position.z +
            baycentric.y * t.v1.position.z + baycentric.z *
            t.v2.position.z);
         float storedZ = m_buffer.depthBuffer[index];
        if(finalDepth < storedZ)
            return;
    } else
    {
        return;
    }

    for(int i = 0; i < aliasAmount; i++)
    {
       for(int j = 0; j < aliasAmount; j++)
       {
            p = vec2f(x + i*(1.0f/aliasAmount), y + j*(1.0f/aliasAmount)); 
            if(intersectsTriangle(p,t,&baycentric))
            {
                float z = 1.0f / (baycentric.x * t.v0.position.z +
                        baycentric.y * t.v1.position.z + baycentric.z *
                        t.v2.position.z);

                float storedZ = m_buffer.depthBuffer[index];
                if(z >= storedZ) //perform a less or equal z test
                {
                    clr = t.v0.color * baycentric.x + t.v1.color * baycentric.y + t.v2.color * baycentric.z;
                    texcoord = t.v0.texcoord * baycentric.x + t.v1.texcoord * baycentric.y + t.v2.texcoord * baycentric.z;
#ifdef PERSP_CORRECT_INTERP
                    clr *= z;
                    texcoord *= z;
#endif 
                    //proof of concept texture.
                    int M = 10;
                    float checker = (fmodf(texcoord.x * M, 1.0f) > 0.5f) ^ (fmodf(texcoord.y * M, 1.0f) < 0.5f);
                    float c = 0.3f * (1 - checker) + 0.7 * checker;

                    finalClr += clr * c;
                }
            }
       }
    }

    finalClr *= (1.0f/(aliasAmount*aliasAmount));

    m_buffer.colorBuffer[index] = finalClr;
    m_buffer.depthBuffer[index] = finalDepth;
}
void Renderer::clearColor(const float& r, const float& g, const float& b,
        const float& a)
{
    m_clearColor = vec4f(clampf(r, 0, 1),clampf(g, 0, 1),clampf(b, 0, 1),
            clampf(a, 0, 1));
}
