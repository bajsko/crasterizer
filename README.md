# crasterizer - CPU Rasterizer

A CPU based triangle rasterizer featuring perspective correct vertex attribute interpolation

This is not meant to be the most efficient rasterizer out there. It is meant
only to showcase the basic algorithms and mathematics behind it, a proof of concept
if you will.

## Features
- Perspective correct vertex attribute interpolation.
- Fixed Function pipeline featuring position, color and uv coordinates.
- Wireframe or fill render (TODO)
- Antialiasing, from 2x2 up to 16x16.
- Written in C++11 using only the standard library.

## Note
While UV coordinates are correctly mapped, there is currently no support for
loading in textures from images. A checkerboard pattern is used to demonstrate the functionallity.

## Building
This project uses CMake. To build, simply clone the repository and make a new directory
where you would like to build the project, then call `cmake <PATH_TO_CRASTERIZER>` 
inside that directory to generate makefiles for your system.