/*
 * crasterizer
 * camera.h
 *
 * Description: The camera is used by the renderer to describe how
 * the 3D world should be rendered to the screen. 
 * 
 * Created by Klas Henriksson on 2019/08/19. All right reserved.
 */

#ifndef _CAMERA_H
#define _CAMERA_H

#include "crast_math.h"

class Camera
{
    public:
        /* View Matrix, defining world relative to camera space */
        mat44f viewMat;
        /* Projection Matrix, defining how to project camera space coordinates 
         * to clip space (clip space is 2x2x2 cube) */
        mat44f projectionMat;
        /* The near and far clipping planes */
        float near, far;

        /* Constructs a new camera object with default settings,
         * identity view mat (positioned at origin, pointing forward)
         * projection matrix is set to perspective projection with 60 fov,
         * 0.1 near plane and 100 far plane.
         */ 
        Camera() : viewMat(), projectionMat(Q_perspective(60.0f, 4.0f/3.0f, 
                    0.1f, 100.0f)), near(0.1f), far(100.0f) {}
        
        Camera(const mat44f& viewMat, const mat44f& projMat, 
            const vec2i& screenDim, const float& n, const float& f) :
            viewMat(viewMat), projectionMat(projMat), near(n), far(f) {}

        ~Camera() {}

        void setPosition(const vec3f& pos) { viewMat[0][3] = -pos.x; viewMat[1][3] = -pos.y; viewMat[2][3] = -pos.z; }
        vec3f getPosition() const { return vec3f(viewMat[0][3], viewMat[1][3], viewMat[2][3]); }
};

#endif
