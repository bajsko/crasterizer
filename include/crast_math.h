 /*
 *	crasterizer
 *  (This is a stripped down part of the qmath.h file from the Quick Game
 *  Engine).
 *	File: crast_math.h
 *	Description: Required math used across all modules
 *				Note: Matricies are considered column major.
 *					  That is, during vector-matrix multiplication, the vector is considered a [1x4] matrix
 *					  Internally the memory is laid out in row-major.
 *					  Rotations are according to the right hand rule.
 *
 *	Created on 2017/07/17
 *	Copyright (c) Quick Dev Team & bajsko. All rights reserved.
 */

#ifndef _QUICKMATH_H
#define _QUICKMATH_H

#define MATH_CMP_EPSILON 1e-5

#include <math.h>

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

#include "qtypes.h"

#define TO_RAD(x) (float)(x * M_PI/180.0f)
#define TO_DEG(x) (float)(x * 180.0f/M_PI)

#define MATH_SHOULD_ROUND 0

#define clampf clamp<float>

#define convertToZeroOner(x,min,max) convertToRange<float>(x,min,max,0,1)

#define lerpf lerp<float>

//========================
//	CLASSESS
//========================

template<typename T>
class Vec4
{
public:
	Vec4() : x(0), y(0), z(0), w(0) {}
	Vec4(const T& x) : x(x), y(x), z(x), w(x) {}
	Vec4(const T& xx, const T& yy, const T& zz, const T& ww) : x(xx), y(yy), z(zz), w(ww) {}
	Vec4(const Vec4<T>& v) : x(v.x), y(v.y), z(v.z), w(v.w) {}

	Vec4<T> operator + (const Vec4<T>& v) const { return Vec4<T>(x + v.x, y + v.y, z + v.z, w + v.w); }
	Vec4<T> operator - (const Vec4<T>& v) const { return Vec4<T>(x - v.x, y - v.y, z - v.z, w - v.w); }
	Vec4<T> operator * (const Vec4<T>& v) const { return Vec4<T>(x * v.x, y * v.y, z * v.z, w * v.w); }
	Vec4<T> operator * (const T& s) const { return Vec4<T>(x * s, y * s, z * s, w * s); }

	void operator += (const Vec4<T>& v) { x += v.x; y += v.y; z += v.z; w += v.w; }
	void operator -= (const Vec4<T>& v) { x -= v.x; y -= v.y; z -= v.z; w -= v.w; }
	void operator *= (const Vec4<T>& v) { x *= v.x; y *= v.y; z *= v.z; w *= v.w; }
	void operator *= (const T& s) { x *= s; y *= s; z *= s; w *= s; }
	bool operator == (const Vec4<T>& v) const { return x == v.x && y == v.y && z == v.z && w == v.w; }
    bool operator != (const Vec4<T>& v) const { return x != v.x || y != v.y || z != v.z || w != v.w; }
    T& operator [] (const int& index) 
    {
        switch (index)
        {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        case 3:
            return w;
        default:
            return x;
        }
    }

	T dot(const Vec4<T>& v) const
	{
		return x * v.x + y * v.y + z * v.z + w * v.w;
	}

	T length() const
	{
		return (T)sqrt(x * x + y * y + z * z + w * w);
	}

	T lengthSquared() const
	{
		return (T)(x*x + y*y + z*z + w*w);
	}

	Vec4<T> normalized() const
	{
		T len = length();
		if (len == 0)
			return Vec4<T>(0);
		return Vec4<T>(x / len, y / len, z / len, w / len);
	}

	void normalize()
	{
		T len = length();
		if (len == 0)
			return;
		x /= len;
		y /= len;
		z /= len;
		w /= len;
	}

	T x, y, z, w;
};

template<typename T>
class Vec3
{
public:
	Vec3() : x(0), y(0), z(0) {}
	Vec3(const T& x) : x(x), y(x), z(x) {}
	Vec3(const T& xx, const T& yy, const T& zz) : x(xx), y(yy), z(zz) {}
	Vec3(const Vec3<T>& v) : x(v.x), y(v.y), z(v.z) {}

	Vec3<T> operator + (const Vec3<T>& v) const { return Vec3<T>(x + v.x, y + v.y, z + v.z); }
	Vec3<T> operator - (const Vec3<T>& v) const { return Vec3<T>(x - v.x, y - v.y, z - v.z); }
	Vec3<T> operator * (const Vec3<T>& v) const { return Vec3<T>(x * v.x, y * v.y, z * v.z); }
	Vec3<T> operator * (const T& s) const { return Vec3<T>(x * s, y * s, z * s); }

	void operator += (const Vec3<T>& v) { x += v.x; y += v.y; z += v.z; }
	void operator -= (const Vec3<T>& v) { x -= v.x; y -= v.y; z -= v.z; }
	void operator *= (const Vec3<T>& v) { x *= v.x; y *= v.y; z *= v.z; }
	void operator *= (const T& s) { x *= s; y *= s; z *= s; }
	bool operator == (const Vec3<T>& v) const { return x == v.x && y == v.y && z == v.z; }
    bool operator != (const Vec3<T>& v) const { return x != v.x || y != v.y || z != v.z;}
    T& operator [] (const int& index)
    {
        switch (index)
        {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            return x;
        }
    }

	T dot(const Vec3<T>& v) const
	{
		return x * v.x + y * v.y + z * v.z;
	}

	Vec3<T> cross(const Vec3<T>& v) const
	{
		return Vec3<T>(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	T length() const
	{
		return (T)sqrt(x * x + y * y + z * z);
	}

	T lengthSquared() const
	{
		return (T)(x*x + y*y + z*z);
	}

	Vec3<T> normalized() const
	{
		T len = length();
		if (len == 0)
			return Vec3<T>();

		return Vec3<T>(x / len, y / len, z / len);
	}

	void normalize()
	{
		T len = length();
		if (len == 0)
			return;
		x /= len;
		y /= len;
		z /= len;
	}

	T x, y, z;
};

template<typename T>
class Vec2
{
public:
	Vec2() : x(0), y(0) {}
	Vec2(const T& x) : x(x), y(x) {}
	Vec2(const T& xx, const T& yy) : x(xx), y(yy) {}

	Vec2<T> operator + (const Vec2<T>& v) const { return Vec2<T>(x + v.x, y + v.y); }
	Vec2<T> operator - (const Vec2<T>& v) const { return Vec2<T>(x - v.x, y - v.y); }
	Vec2<T> operator * (const Vec2<T>& v) const { return Vec2<T>(x * v.x, y * v.y); }
	Vec2<T> operator * (const T& s) const { return Vec2<T>(x * s, y * s); }
	bool operator == (const Vec2<T>& v) const { return x == v.x && y == v.y; }
    bool operator != (const Vec2<T>& v) const { return x != v.x || y != v.y; }

	void operator += (const Vec2<T>& v) { x += v.x; y += v.y; }
	void operator -= (const Vec2<T>& v) { x -= v.x; y -= v.y; }
	void operator *= (const Vec2<T>& v) { x *= v.x; y *= v.y; }
	void operator *= (const T& s) { x *= s; y *= s; }
    T& operator [] (const int& index)
    {
        switch (index)
        {
        case 0:
            return x;
        case 1:
            return y;
        default:
            return x;
        }
    }

	T dot(const Vec2<T>& v) const
	{
		return x * v.x + y * v.y;
	}

	T length() const
	{
		return (T)sqrt(x * x + y * y);
	}

	T lengthSquared() const
	{
		return (T)(x*x + y*y);
	}

	Vec2<T> normalized() const
	{
		T len = length();
		if (len == 0)
			return Vec2<T>();

		return Vec2<T>(x / len, y / len);
	}

	void normalize()
	{
		T len = length();
		if (len == 0)
			return;
		x /= len;
		y /= len;
	}

	T x, y;
};

//column matrix
template<typename T>
class Matrix4x4
{
public:
	Matrix4x4() {}
	Matrix4x4(const T& diag) { m[0][0] = m[1][1] = m[2][2] = m[3][3] = diag; }
	Matrix4x4(const Vec4<T>& diag) { m[0][0] = diag.x; m[1][1] = diag.y; m[2][2] = diag.z; m[3][3] = diag.w; }
	Matrix4x4(T* const &vals) { memcpy(m, vals, 16 * sizeof(T)); }
	Matrix4x4(const Matrix4x4<T>& mat) { memcpy(m, mat.m, 16 * sizeof(T)); }

	Matrix4x4<T> multMatrix(const Matrix4x4<T>& rightSide) const
	{
		T vals[16] = { 0 };
		T rightCol[4] = { 0 };

		const T* leftRow;
		const T (*rsvals)[4] = rightSide.m;
		for (int i = 0; i < 4; i++) //rows
		{
			leftRow = &m[i][0];
			for (int j = 0; j < 4; j++) //columns
			{
				rightCol[0] = rsvals[0][j];
				rightCol[1] = rsvals[1][j];
				rightCol[2] = rsvals[2][j];
				rightCol[3] = rsvals[3][j];
				vals[j + i * 4] = (leftRow[0] * rightCol[0]) + (leftRow[1] * rightCol[1]) + (leftRow[2] * rightCol[2]) + (leftRow[3] * rightCol[3]);
#if MATH_SHOULD_ROUND == 1
				vals[j + i * 4] = (T)round(vals[j + i * 4]);
#endif
			}
		}
		return Matrix4x4<T>(vals);
	}

	Matrix4x4<T> operator * (const Matrix4x4<T>& rightSide) const { return multMatrix(rightSide); }

	Vec3<T> multVec(const Vec3<T>& src) const
	{
		Vec3<T> v;
		v.x = src.x * m[0][0] + src.y * m[0][1] + src.z * m[0][2] + m[0][3];
		v.y = src.x * m[1][0] + src.y * m[1][1] + src.z * m[1][2] + m[1][3];
		v.z = src.x * m[2][0] + src.y * m[2][1] + src.z * m[2][2] + m[2][3];
		T w = src.x * m[3][0] + src.y * m[3][1] + src.z * m[3][2] + m[3][3];
		if (w != 1 && w != 0)
		{
			T oneOverW = (T)1 / w;
			v.x *= oneOverW;
			v.y *= oneOverW;
			v.z *= oneOverW;
		}
		return v;
	}

	Vec3<T> multDirVec(const Vec3<T>& src) const
	{
		Vec3<T> v;
		v.x = src.x * m[0][0] + src.y * m[0][1] + src.z * m[0][2];
		v.y = src.x * m[1][0] + src.y * m[1][1] + src.z * m[1][2];
		v.z = src.x * m[2][0] + src.y * m[2][1] + src.z * m[2][2];
		return v;
	}
    
    Vec4<T> multVec(const Vec4<T>& src) const
    {
        Vec4<T> v;
        v.x = src.x * m[0][0] + src.y * m[0][1] + src.z * m[0][2] + src.w * m[0][3];
        v.y = src.x * m[1][0] + src.y * m[1][1] + src.z * m[1][2] + src.w * m[1][3];
        v.z = src.x * m[2][0] + src.y * m[2][1] + src.z * m[2][2] + src.w * m[2][3];
        v.w = src.x * m[3][0] + src.y * m[3][1] + src.z * m[3][2] + src.w * m[3][3];
        return v;
    }

	Matrix4x4<T> inverted() const
	{
		Matrix4x4<T> orig(*this);
		Matrix4x4<T> ret;
		size_t rowSize = sizeof(T) * 4;
		int zeroRow = 0;
		
		for (int row = 0; row < 3; row++)
		{
			if (orig[row][row] == 0)
			{
				zeroRow = row;
				for (int i = 0; i < 4; i++)
					if ((T)fabs(orig[i][i]) > (T)fabs(orig[row][row])) zeroRow = i;
				if (zeroRow == row)
					return ret;
				memswap(&orig[row][0], &orig[zeroRow][0], rowSize);
				memswap(&ret[row][0], &ret[zeroRow][0], rowSize);
			}
		}

		for (int row = 0; row < 4; row++)
		{
			T& pivot = orig[row][row];
			if (pivot != 1)
			{
				T coeff = (T)1 / pivot;
				orig[row][0] *= coeff;
				orig[row][1] *= coeff;
				orig[row][2] *= coeff;
				orig[row][3] *= coeff;

				ret[row][0] *= coeff;
				ret[row][1] *= coeff;
				ret[row][2] *= coeff;
				ret[row][3] *= coeff;
			}

			for (int rb = row + 1; rb < 4; rb++)
			{
				T coeff = orig[rb][row];
				if (coeff != 0)
				{
					orig[rb][0] -= coeff * orig[row][0];
					orig[rb][1] -= coeff * orig[row][1];
					orig[rb][2] -= coeff * orig[row][2];
					orig[rb][3] -= coeff * orig[row][3];

					ret[rb][0] -= coeff * ret[row][0];
					ret[rb][1] -= coeff * ret[row][1];
					ret[rb][2] -= coeff * ret[row][2];
					ret[rb][3] -= coeff * ret[row][3];
				}

				//orig[rb][row] = ret[rb][row] = 0;
			}
		}

		for (int row = 3; row > 0; row--)
		{
			for (int rb = row - 1; rb >= 0; rb--)
			{
				T coeff = orig[rb][row];
				if (coeff != 0)
				{
					orig[rb][0] -= coeff * orig[row][0];
					orig[rb][1] -= coeff * orig[row][1];
					orig[rb][2] -= coeff * orig[row][2];
					orig[rb][3] -= coeff * orig[row][3];

					ret[rb][0] -= coeff * ret[row][0];
					ret[rb][1] -= coeff * ret[row][1];
					ret[rb][2] -= coeff * ret[row][2];
					ret[rb][3] -= coeff * ret[row][3];
				}
			}

		}

		return ret;
	}

	void invert()
	{
		*this = inverted();
	}

	bool operator == (const Matrix4x4<T>& b) const
	{
		const T (*bm)[4] = b.m;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				T diff = fabsf(m[i][j] - bm[i][j]);
				if (diff > MATH_CMP_EPSILON)
					return false;
			}
		}
		return true;
	}

	void setRow(const int& index, const T& a, const T& b, const T& c, const T& d)
	{
		if (index < 0 || index > 3)
			return;
		m[index][0] = a;
		m[index][1] = b;
		m[index][2] = c;
		m[index][3] = d;
	}

	/// <summary>
	/// Returns the top 3x3 part of this matrix (the one that encodes rotation) and sets last col to 0 0 0 1
	/// </summary>
	/// <returns>Returns the top 3x3 part of this matrix (the one that encodes rotation) and sets last col to 0 0 0 1</returns>
	Matrix4x4<T> rotation() const
	{
		Matrix4x4<T> ret = *this;
		ret.m[0][3] = ret.m[1][3] = ret.m[2][3] = 0;
		ret.m[3][3] = 1;

		return ret;
	}

	/// <summary>
	/// Transposes this matrix.
	/// </summary>
	void transpose()
	{
		*this = this->transposed();
	}

	/// <summary>
	/// </summary>
	/// <returns>Returns transposed matrix</returns>
	Matrix4x4<T> transposed() const
	{
		Matrix4x4<T> cpy = *this;
		Matrix4x4<T> ret;
		for (int row = 0; row < 4; row++)
		{
			ret[row][0] = cpy[0][row];
			ret[row][1] = cpy[1][row];
			ret[row][2] = cpy[2][row];
			ret[row][3] = cpy[3][row];
		}

		return ret;
	}

	const T* operator[] (const uint8_t& i) const { return &m[i][0]; }
	T* operator[] (const uint8_t& i) { return &m[i][0]; }

	T m[4][4] = { { 1, 0, 0, 0 }, { 0, 1, 0, 0}, { 0,0,1,0}, {0,0,0,1} };
};


template<typename T>
class Quaternion
{
public:
	Quaternion() : rep(0) {};
	Quaternion(const Vec3<T>& v, const T& angle) : rep(v.x, v.y, v.z, 0)
	{
		T sinAng = angle == 0 ? 0 : (T)sinf(TO_RAD(angle / 2));
		T cosAng = angle == 0 ? 1 : (T)cosf(TO_RAD(angle / 2));
		rep.normalize();
		rep *= sinAng;
		rep.w = cosAng;
	}

	Quaternion(const Vec4<T>& quatDat) : rep(quatDat) {}
	Quaternion(const T& x, const T& y, const T& z, const T& w) : rep(x, y, z, w) {}

	//p*q
	Quaternion<T> mult(const Quaternion<T>& q) const
	{
		Vec3<T> pAxis(rep.x, rep.y, rep.z);
		Vec3<T> qAxis(q.rep.x, q.rep.y, q.rep.z);

		Vec3<T> pSqAxis(pAxis * q.rep.w);
		Vec3<T> qSpAxis(qAxis * rep.w);
		Vec3<T> axisMod(pSqAxis + qSpAxis + (pAxis.cross(qAxis)));
		T pSqS = (rep.w * q.rep.w) - pAxis.dot(qAxis);

		return Quaternion<T>(Vec4<T>(axisMod.x, axisMod.y, axisMod.z, pSqS));
	}

	Quaternion<T> conjugate() const 
	{
		Vec3<T> axis(rep.x, rep.y, rep.z);
		axis *= -1;
		return Quaternion<T>(Vec4<T>(axis.x, axis.y, axis.z, rep.w));
	}

	Quaternion<T> operator * (const Quaternion<T>& p) const { return mult(p); }

	//rotPoint
	Vec3<T> operator * (const Vec3<T>& p) const
	{
		Quaternion<T> vquat(p.x, p.y, p.z, 0);
		Quaternion<T> conj = conjugate();
		
		Quaternion<T> qv = *this * vquat;
		Quaternion<T> qvqc = qv * conj;
		return Vec3<T>(qvqc.rep.x, qvqc.rep.y, qvqc.rep.z);
	}

	bool operator == (const Quaternion<T>& p) const { return rep == p.rep; }

	Quaternion<T> slerp(const Quaternion<T>& b, const T& t) const
	{
		Quaternion<T> q = *this;
		Quaternion<T> bqInverse = b*q.conjugate();

		float angleDeg = TO_DEG(acos(bqInverse.rep.w) * 2) * t;
		Vec3<T> axis = Vec3<T>(bqInverse.rep.x, bqInverse.rep.y, bqInverse.rep.z).normalized();

		Quaternion<T> intermediate = Quaternion<T>(axis, angleDeg);
		return intermediate * q;
	}

	T length() const
	{
		return rep.length();
	}

	T lengthSquared() const
	{
		return rep.lengthSquared();
	}

	Quaternion<T> normalized() const
	{
		return Quaternion<T>(rep.normalized());
	}

	Matrix4x4<T> toMatrix() const 
	{
		Quaternion<T> p = this->normalized();

		real sqw = p.rep.w * p.rep.w;
		real sqx = p.rep.x * p.rep.x;
		real sqy = p.rep.y * p.rep.y;
		real sqz = p.rep.z * p.rep.z;

		real m00 = (sqx - sqy - sqz + sqw);
		real m11 = (-sqx + sqy - sqz + sqw);
		real m22 = (-sqx - sqy + sqz + sqw);

		real tmp1 = p.rep.x * p.rep.y;
		real tmp2 = p.rep.z * p.rep.w;
		real m10 = 2 * (tmp1 + tmp2);
		real m01 = 2 * (tmp1 - tmp2);

		tmp1 = p.rep.x * p.rep.z;
		tmp2 = p.rep.y * p.rep.w;
		real m20 = 2 * (tmp1 - tmp2);
		real m02 = 2 * (tmp1 + tmp2);

		tmp1 = p.rep.y * p.rep.z;
		tmp2 = p.rep.x * p.rep.w;
		real m21 = 2 * (tmp1 + tmp2);
		real m12 = 2 * (tmp1 - tmp2);

		Matrix4x4<T> ret;

		ret.setRow(0, m00, m01, m02, 0);
		ret.setRow(1, m10, m11, m12, 0);
		ret.setRow(2, m20, m21, m22, 0);

		return ret;
	}

	/*static Quaternion<T> fromMatrix(const Matrix4x4<T>& rotMat) //todo
	{
		Quaternion<T> quat;

		T trace = rotMat[0][0] + rotMat[1][1] + rotMat[2][2];
		if (trace > (T)0.0)
		{
			T s = (T)sqrt(trace + 1.0);
			quat.rep.w = s * 0.5f;

			T t = 0.5f / s;
			quat.rep.x = (rotMat[2][1] - rotMat[1][2]) * t;
			quat.rep.y = (rotMat[0][2] - rotMat[2][0]) * t;
			quat.rep.z = (rotMat[1][0] - rotMat[0][1]) * t;
		}
		else //diag is negative
		{
			int i = 0;
			if (rotMat[1][1] > rotMat[0][0]) i = 1;
			if (rotMat[2][2] > rotMat[i][i]) i = 2;

			int NEXT[3] = { 1,2,0 };
			int j = NEXT[i];
			int k = NEXT[j];
			T s = (T)sqrt((rotMat[i][j] - (rotMat[j][j] + rotMat[k][k])) + 1.0f);

			
		}
	}*/
public:
	Vec4<T> rep;
};

//========================
// UTILITY FUNCTIONS
//========================

template<typename T>
T clamp(const T& val, const T& l, const T& h)
{
	if (val < l) return l;
	if (val > h) return h;
	return val;
}

template<typename T>
Vec3<T> clampvec3(const Vec3<T>& v, const real& maxMag)
{
	if (v.lengthSquared() < maxMag*maxMag)
		return v;
	Vec3<T> ret = v.normalized();
	ret *= maxMag;
	return ret;
}

inline real dot(const Vec3<real>& a, const Vec3<real>& b)
{
	return a.dot(b);
}

//perfors A cross B
inline Vec3<real> cross(const Vec3<real>& a, const Vec3<real>& b)
{
	return a.cross(b);
}

template<typename T, typename A>
inline Vec4<T> convert_vec4(const Vec4<A>& v)
{
    return Vec4<T>((T)v.x, (T)v.y, (T)v.z, (T)v.w);
}

template<typename T, typename A>
inline Vec3<T> convert_vec3(const Vec3<A>& v)
{
    return Vec3<T>((T)v.x, (T)v.y, (T)v.z);
}

template<typename T, typename A>
inline Vec2<T> convert_vec2(const Vec2<A>& v)
{
    return Vec2<T>((T)v.x, (T)v.y);
}

//------------
// create a quat
// that rotates degX around x axis
// degY around y axis and degZ around z axis
// in order x->y->z
//------------
inline Quaternion<real> Q_quatFromEuler(const real& degX, const real& degY, const real& degZ)
{
	Quaternion<real> x(Vec3<real>(1,0,0), degX);
	Quaternion<real> y(Vec3<real>(0, 1, 0), degY);
	Quaternion<real> z(Vec3<real>(0, 0, 1), degZ);
	return x * y * z;
}

inline Quaternion<real> slerp(const Quaternion<real>& a, const Quaternion<real>& b, const real& t)
{
	real tClamped = clamp<real>(t, 0, 1);
	return a.slerp(b, tClamped);
}

//finds the quaternion that rotates a to point towards b
inline Quaternion<real> Q_findRotation(const Vec3<real>& a, const Vec3<real>& b)
{
	Quaternion<real> q;

	real dotab = dot(a, b);

	if (dotab >= 1)
	{
		q = Quaternion<real>(0, 0, 0, 1);
	}
	else if (dotab < -0.999999f)
	{
		Vec3<real> axis = cross(Vec3<real>(0,1,0), b);

		if (axis.length() == 0)
			axis = cross(Vec3<real>(1, 0, 0), b);
		axis.normalize();
		q = Quaternion<real>(axis, 180);
	}
	else
	{
		Vec3<real> c = cross(a, b);
		real s = sqrtf((1 + dotab) * 2);
		real invs = 1 / s;

		q.rep.x = c.x * invs;
		q.rep.y = c.y * invs;
		q.rep.z = c.z * invs;
		q.rep.w = s * 0.5f;
	}

	q.normalized();

	return q;
}

inline Matrix4x4<real> Q_perspective(const real& degFov, const real& aspect, const real& n, const real& f)
{
	real halfFov = TO_RAD((degFov / 2.0f));
    real tanHalfFov = tanf(halfFov);

	Matrix4x4<real> projMat;

    projMat[0][0] = (real)(1/(aspect * tanHalfFov));

    projMat[1][1] = (real)(1/tanHalfFov);

	projMat[2][2] = -((f + n) / (f - n));
	projMat[2][3] = (real)(-(((real)2 * f*n) / (f - n)));
	projMat[3][2] = (real)-1;

	return projMat;
}

inline Matrix4x4<real> Q_ortho(const real& b, const real& t, const real& l, const real& r, const real& n, const real& f)
{
	Matrix4x4<real> orthoMat;
	
	orthoMat[0][0] = 2 / (r - l);
	orthoMat[0][3] = -((r + l) / (r - l));

	orthoMat[1][1] = 2 / (t - b);
	orthoMat[1][3] = -((t + b) / (t - b));
	
	orthoMat[2][2] = -2 / (f - n);
	orthoMat[2][3] = -((f + n) / (f - n));
	
	orthoMat[3][3] = 1;

	return orthoMat;
}

inline Matrix4x4<real> Q_rotateX(const real& angDeg)
{
	real ang = TO_RAD(angDeg);
	Matrix4x4<real> ret;
	ret[1][1] = cos(ang);
	ret[1][2] = -sin(ang);
	ret[2][1] = sin(ang);
	ret[2][2] = cos(ang);
	return ret;
}

inline Matrix4x4<real> Q_rotateY(const real& angDeg)
{
	real ang = TO_RAD(angDeg);
	Matrix4x4<real> ret;
	ret[0][0] = cos(ang);
	ret[0][2] = sin(ang);
	ret[2][0] = -sin(ang);
	ret[2][2] = cos(ang);
	return ret;
}

inline Matrix4x4<real> Q_rotateZ(const real& angDeg)
{
	real ang = TO_RAD(angDeg);
	Matrix4x4<real> ret;
	ret[0][0] = cos(ang);
	ret[0][1] = -sin(ang);
	ret[1][0] = sin(ang);
	ret[1][1] = cos(ang);
	return ret;
}

inline Matrix4x4<real> Q_translate(const Vec3<real>& v)
{
	Matrix4x4<real> ret;
	ret[0][3] = v.x;
	ret[1][3] = v.y;
	ret[2][3] = v.z;
	return ret;
}

inline Matrix4x4<real> Q_scale(const real& v)
{
	Matrix4x4<real> ret;
	ret[0][0] = v;
	ret[1][1] = v;
	ret[2][2] = v;
	return ret;
}

inline Matrix4x4<real> Q_scale(const Vec3<real>& v)
{
	Matrix4x4<real> ret;
	ret[0][0] = v.x;
	ret[1][1] = v.y;
	ret[2][2] = v.z;
	return ret;
}

//inverse of a ordinary modelview transform
inline Matrix4x4<real> Q_lookAt(const Vec3<real>& from, const Vec3<real>& to, const Vec3<real>& tmp = Vec3<real>(0.0f, 1.0f, 0.0f))
{
	Vec3<real> forward = (to - from).normalized();
	Vec3<real> up = tmp;
    Vec3<real> right = forward.cross(up);
    up = right.cross(forward);

	Matrix4x4<real> view;

	view[0][0] = right.x;
	view[0][1] = right.y;
	view[0][2] = right.z;

	view[1][0] = up.x;
	view[1][1] = up.y;
	view[1][2] = up.z;

	view[2][0] = forward.x;
	view[2][1] = forward.y;
	view[2][2] = forward.z;
    
	view[0][3] = -right.dot(from);
	view[1][3] = -up.dot(from);
	view[2][3] = forward.dot(from);

	return view;
}

template<typename T>
inline T convertToRange(const T& val, const T& oldMin, const T& oldMax, const T& newMin, const T& newMax)
{
    real oldDiff = oldMax - oldMin;
    real val01 = val / oldDiff;
    
    real newDiff = newMax - newMin;
    real retVal = val01 * newDiff;
    retVal += newMin;
    
    return retVal;
}

template<typename T>
inline T lerp(const T& low, const T& high, T t)
{
    t = clamp<T>(t,0,1);
    return (1-t) * low + t * high;
}

template<typename T>
inline Vec4<T> lerp(const Vec4<T>& a, const Vec4<T>& b, T t)
{
    t = clamp<T>(t,0,1);
    return a + (b-a)*t;
}

template<typename T>
inline Vec3<T> lerp(const Vec3<T>& a, const Vec3<T>& b, T t)
{
    t = clamp<T>(t,0,1);
    return a + (b-a)*t;
}

template<typename T>
inline Vec2<T> lerp(const Vec2<T>& a, const Vec2<T>& b, T t)
{
    t = clamp<T>(t,0,1);
    return a + (b-a)*t;
}

template<typename T>
inline Vec4<T> floor(const Vec4<T>& v)
{
    return Vec4<T>(floor(v.x), floor(v.y), floor(v.z), floor(v.w));
}

template<typename T>
inline Vec3<T> floor(const Vec3<T>& v)
{
    return Vec3<T>(floor(v.x), floor(v.y), floor(v.z));
}

template<typename T>
inline Vec2<T> floor(const Vec2<T>& v)
{
    return Vec2<T>(floor(v.x), floor(v.y));
}

template<typename T>
inline Vec4<T> ceil(const Vec4<T>& v)
{
    return Vec4<T>(ceil(v.x), ceil(v.y), ceil(v.z), ceil(v.w));
}

template<typename T>
inline Vec3<T> ceil(const Vec3<T>& v)
{
    return Vec3<T>(ceil(v.x), ceil(v.y), ceil(v.z));
}

template<typename T>
inline Vec2<T> ceil(const Vec2<T>& v)
{
    return Vec2<T>(ceil(v.x), ceil(v.y));
}

template<typename T>
inline Vec4<T> abs(const Vec4<T>& v)
{
    return Vec4<T>(abs(v.x), abs(v.y), abs(v.z), abs(v.w));
}

template<typename T>
inline Vec3<T> abs(const Vec3<T>& v)
{
    return Vec3<T>(abs(v.x), abs(v.y), abs(v.z));
}

template<typename T>
inline Vec2<T> abs(const Vec2<T>& v)
{
    return Vec2<T>(abs(v.x), abs(v.y));
}

//========================
// TYPEDEFS AND CONSTS
//========================

typedef Vec4<real> vec4;
typedef Vec4<float> vec4f;
typedef Vec4<double> vec4d;
typedef Vec4<int> vec4i;

typedef Vec3<real> vec3;
typedef Vec3<float> vec3f;
typedef Vec3<double> vec3d;
typedef Vec3<int> vec3i;

typedef Vec2<real> vec2;
typedef Vec2<float> vec2f;
typedef Vec2<double> vec2d;
typedef Vec2<int> vec2i;
typedef Vec2<uint32_t> vec2ui;

typedef Matrix4x4<real> mat44;
typedef Matrix4x4<float> mat44f;
typedef Matrix4x4<double> mat44d;

typedef Quaternion<real> quat;
typedef Quaternion<float> quatf;
typedef Quaternion<double> quatd;

enum EColorInterpolation
{
	ColorInterp_Linear,
};

/// <summary>
/// Exact same thing as a vec3 but all components are scaled with 1/255 (to convert to 0-1)
/// </summary>
class QColor
{
public:
	vec3 clr;

	QColor() {}
	QColor(const float& r, const float& g, const float& b) : clr(r / 255.0f, g / 255.0f, b / 255.0f) {}
	QColor(const vec3& rgbClr) : clr(rgbClr * (1 / 255.0f)) {}
	QColor(const uint32_t& hex) 
	{
		real r = (real)((hex & 0xFF0000) >> 16);
		real g = (real)((hex & 0xFF00) >> 8);
		real b = (real)(hex & 0xFF);

		r = convertToZeroOner(r, 0, 255);
		g = convertToZeroOner(g, 0, 255);
		b = convertToZeroOner(b, 0, 255);

		clr.x = r;
		clr.y = g;
		clr.z = b;
	}
};

/// <summary>
/// Interpolates between color a and color b with value val
/// </summary>
/// <param name="a">First Color</param>
/// <param name="b">Second Color</param>
/// <param name="val">How much to interpolate, 0 = a, 1 = b</param>
/// <param name="interpType">Interpolation type</param>
/// <returns></returns>
inline QColor Q_colorInterpolate(const QColor& a, const QColor& b, const float& val, const EColorInterpolation& interpType = ColorInterp_Linear)
{
	vec3 diff = b.clr - a.clr;

	vec3 newClr;
	if (interpType == ColorInterp_Linear)
		newClr = a.clr + diff * val;

	QColor ret;
	ret.clr = newClr;
	return ret;
}

inline QColor Q_colorFromHex(const uint32_t& hex)
{
	real r = (real)((hex & 0xFF0000) >> 16);
	real g = (real)((hex & 0xFF00) >> 8);
	real b = (real)(hex & 0xFF);

	r = convertToZeroOner(r, 0, 255);
	g = convertToZeroOner(g, 0, 255);
	b = convertToZeroOner(b, 0, 255);
	
	QColor ret;
	ret.clr = vec3(r, g, b);
	return ret;
}

const QColor kColorRed = QColor(255, 0, 0);
const QColor kColorGreen = QColor(0, 255, 0);
const QColor kColorBlue = QColor(0, 0, 255);
const QColor kColorBlack = QColor(0, 0, 0);
const QColor kColorWhite = QColor(255, 255, 255);

#endif
