/*
*	crasterizer
*
*	File: qtypes.h (this file is a modified version of qtypes.h from the Quick
*	Game Engine.)
*	Description: Contains typedefs
*
*	Created on 2017/07/17
*	Copyright (c) Quick Dev Team & bajsko. All rights reserved.
*/

#ifndef _QUICK_TYPES_H
#define _QUICK_TYPES_H

#include <iostream>
#include <vector>
#include <string>
#include <stdint.h>
#include <memory>

#define CRAST_OK 1
#define CRAST_FAIL -1

typedef unsigned char uchar;

#ifndef Q_SIGNALS
typedef uint64_t ulong;
#endif

typedef int64_t qlong;

typedef std::string qstring;

typedef float real;

#endif
