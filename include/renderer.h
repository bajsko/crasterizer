/*
 * crasterizer
 * renderer.h
 *
 * Description: The renderer is the main module of the crasterizer. The user
 * pushes vertices foring triangles to the renderer which then rasterizes them
 * to the screen buffers (depth & color) 
 *
 * Created by Klas Henriksson on 2019/08/19. All right reserved.
 */

#ifndef _RENDERER_H
#define _RENDERER_H

#include "camera.h"

#include <vector>

const int kCRasterizerColorBufferBit = 0x1;
const int kCRasterizerDepthBufferBit = 0x2;
const int kCRasterizer4SampleAntiAliasBit = 0x1;
const int kCRasterizer8SampleAntiAliasBit = 0x2;
const int kCRasterizer16SampleAntiAliasBit = 0x3;

/*
 * Fixed Function Pipeline Vertex Structure
 */
struct Vertex
{
    Vertex() : position(0), texcoord(0), color(0) {}
    Vertex(const vec3f& p) : position(p), texcoord(0), color(1) {}
    Vertex(const vec3f& p, const vec2f& t, const vec4f& clr) : position(p),
    texcoord(t), color(clr) {}
    vec3f position;
    vec2f texcoord;
    vec4f color;
};

struct Rectangle
{
    Rectangle() : x(0), y(0), width(0), height(0) {}
    Rectangle(const float& x, const float& y, const float& w, const float& h) 
        : x(x), y(y), width(w), height(h) {}
    float x, y, width, height;
};

struct Triangle
{
    Triangle() : v0(), v1(), v2() {}
    Triangle(const Vertex& v0, const Vertex& v1, const Vertex& v2) : v0(v0),
    v1(v1), v2(v2) {}
    Vertex v0,v1,v2;
};

/* Enumeration describing the order in which vertices are passed */
enum WindingOrder
{
    WO_CCW,
    WO_CW
};

/* Enumeration describing how to render triangles */
enum RenderMode
{
    RM_FILL,
    RM_WIRE
};

/*
 * A render buffer stores the color and depth buffer
 */
struct RenderBuffer
{
    RenderBuffer(const int& width, const int& height) : width(width), 
    height(height), colorBuffer(std::unique_ptr<vec4f[]>
    (new vec4f[width*height])) , depthBuffer(std::unique_ptr<float[]>
    (new float[width*height])) {}
    std::unique_ptr<vec4f[]> colorBuffer;
    std::unique_ptr<float[]> depthBuffer;
    int width, height;
};

class Renderer
{
    private:
        vec4f m_clearColor;
        RenderBuffer m_buffer;
        Rectangle m_viewport;
        int m_flags;

    public:
        /* The model matrix to be applied during fixedRasterization */
        mat44f modelMat;
        /* The camera that the world is rendered from */
        Camera renderCamera;
        /* The winding order expected by the rasterizer */
        WindingOrder windingOrder;
        /* The render mode to use when rasterizing */
        RenderMode renderMode;
    private:
        /* Performs the edge function of the edge (b-a) and the point p, 
         * returning area of parallelogram spawned by (p-a)x(b-a) 
         * taking in consideration the winding order specified. 
         */
        float edgeFunction(const vec2f& a, const vec2f& b, const vec2f& p);
        /* Exact same as above, just to make code easier. */
        float edgeFunction(const vec3f& a, const vec3f& b, const vec2f& p);
        float edgeFunction(const vec3f& a, const vec3f& b, const vec3f& c);
        /* returns if the point lies on or inside the triangle. If not null,
         * baycentric is filled with the baycentric coordinates of the point
         * if it intersects. x coord is for triangle v0, y for v1, z for v2. */
        bool intersectsTriangle(const vec2f& point, const Triangle& triangle, vec3f* const& baycentric);

        /* Computes the minimum bounding rectangle containing triangle */
        Rectangle computeBoundingRect(const Triangle& triangle);

        /* Transforms a vertex that ha been converted to raster space to
         * to account for the miss match that is created during perspective
         * projection. All attributes are divided by the vertex z coord, 
         * after that, the vertex's z coord is set to 1/z.*/ 
        void perspCorrectVertexAttributes(Vertex& v);
        
        /* Converts p, a coordinate in world  space, to raster space.*/
        void convertToRasterSpace(vec3f& p);

        void writeBuffers(const int& x, const int& y, const Triangle& t);

    public:
        /* Initializes a render context with default screen size of
         * 800x600. */
        Renderer();
        ~Renderer() {};
        /* Clears selected buffers */
        void clear(const int& mode);
        /* Sets the color used when calling clear with mode 
         * kCRasterizerColorBufferBit set, all values clamped between [0,1]. */
        void clearColor(const float& r, const float& g, const float& b, const
                float& a);
        int fixedRasterize(const std::vector<Vertex>& vertices);
        
        /* Resizes active renderbuffer to fit accordingly if needed. */
        void setScreenDimensions(const int& w, const int& h);
        /* Sets where on the render buffer to render rasterization.
         * If these are out of bounds, viewport will be set to entire render buffer. */
        void setViewport(const int& x, const int& y, const int& w, const int& h);
        void setViewport(const Rectangle& r); 
        Rectangle getViewport() const { return m_viewport; }

        /* Enable a set of features of the rasterizer. Features
         * can be bitwised OR'd together to enable multiple features at once. */
        void enable(const int& mode);

        /* Disable a set of features of the rasterizer. Features
         * can be bitwised OR'd together to enable multiple features at once. */
        void disable(const int& mode);

        const RenderBuffer& getRenderBuffer() const { return m_buffer; }
};

#endif
