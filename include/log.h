 /* log.h
 *
 * Description: Provides functions to log information to different files/
 * buffers. 
 *
 * Created by Klas Henriksson on 2019/08/20. All right reserved.
 */

#ifndef _CRAST_LOG_H
#define _CRAST_LOG_H

#include "qtypes.h"
#include <stdio.h>

const int kLogModeMain = 0x1;
const int kLogModeBuffer = 0x2;

/* Initializes files and buffers for logging */
void CrastLogInit();
void CrastLog(const int& mode, const qstring& msg);
void CrastLog(const int& mode, const char* format, ...);

#endif
